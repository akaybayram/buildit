#ifndef GAME_HPP__
#define GAME_HPP__

#include <cstdint>
#include <vector>
#include <array>
#include "../inc/clib.hpp"
#include "../inc/player.hpp"
#include "../inc/tile.hpp"
#include "../inc/entity.hpp"

class Game // Singleton Game Object
{
private:
    static Game *instance;
    Game();
    ~Game();
    uint32_t self_id;
    uint32_t width;
    uint32_t height;
    float scale;
    rl::Color color_tint;
    std::string title;
    bool is_running;
public:
    static Game *get_instance();
    rl::Vector2 tile_dim;
    Camera cam;
    std::vector <Player*> players;
    std::vector <Entity*> entities;
    void init(uint32_t width, uint32_t height, std::string title);
    void load_tile_assets();
    void load_entity_assets();
    void mainloop();
    void manage_players();
    void update_players();
    void update(float dt);
    void draw();
    float get_scale();
    uint32_t get_player_id();
    rl::Color get_color_tint();
    std::array<rl::Texture2D, static_cast<int>(TileType::TILE_TYPE_SIZE)> tile_assets;
    std::array<rl::Texture2D, static_cast<int>(EntityType::ENTITY_TYPE_SIZE)> entity_assets;
    std::vector<Tile> tiles;
};

#endif // GAME_HPP__
