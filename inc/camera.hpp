#ifndef CAMERA_HPP__
#define CAMERA_HPP__

#include "../inc/clib.hpp"

class Camera{
    rl::Vector2 pos;
    rl::Vector2 vel;
    float speed;
public:
    Camera();
    ~Camera();
    
    void update(float dt);
    void set_vel(rl::Vector2 vel);
    rl::Vector2 get_pos();
};
#endif // CAMERA_HPP__