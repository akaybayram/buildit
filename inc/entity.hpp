#ifndef ENTITY_HPP__
#define ENTITY_HPP__

#include "../inc/clib.hpp"

enum class EntityType{
    SELECTION,
    HOUSE,
    MARKET,
    ENTITY_TYPE_SIZE
};

//enum class EntityState{
//    FLOATING,
//    STATIC,
//    HIDDEN
//};
//

class Entity;

class EntityState{ // State pattern EntityState abstract class
public:
    EntityState(Entity *en);
    ~EntityState();
    Entity *entity;

    virtual void update(float dt) = 0;
    virtual void draw() = 0;
    // virtual void floating() = 0;
    // virtual void statik() = 0; // static is a keyword
    // virtual void hidden() = 0;
};

class FloatingState: public EntityState{
public:
    FloatingState(Entity *en): EntityState(en) {};

    void update(float dt);
    void draw();
};

class Entity{
public:
    EntityType type;
    rl::Vector2 pos;
    EntityState* state;

    Entity(rl::Vector2 pos, EntityType type);
    ~Entity();
    void update(float dt);
    void draw();
};

#endif // ENTITY_HPP__
