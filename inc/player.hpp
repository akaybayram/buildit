#ifndef PLAYER_HPP__
#define PLAYER_HPP__

#include "../inc/controller.hpp"
#include "../inc/camera.hpp"
#include "../inc/entity.hpp"

class Player{
private:
    Controller controller{};
public:
    Player();
    ~Player();
    Entity *sel;
    rl::Vector2 mouse_pos;
    uint32_t id;
    void handle_controls(); // call head of frames
    void make_updates();    // call in update proc
};
#endif // PLAYER_HPP__
