#ifndef UTIL_HPP__
#define UTIL_HPP__

#include "../inc/clib.hpp"

rl::Vector2 cartesian_to_isometric(rl::Vector2 cart_coord);
rl::Vector2 isometric_to_cartesian(rl::Vector2 iso_coord);
rl::Vector2 operator-(rl::Vector2 a, rl::Vector2 b);
rl::Vector2 operator+(rl::Vector2 a, rl::Vector2 b);
rl::Vector2 operator*(rl::Vector2 a, rl::Vector2 b);
rl::Vector2 operator/(rl::Vector2 a, rl::Vector2 b);

#endif // UTIL_HPP__