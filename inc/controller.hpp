#ifndef CONTROL_HPP_
#define CONTROL_HPP_

#include <cstdio>
#include "../inc/clib.hpp"
class Controller
{
private:
    int32_t cam_left = rl::KEY_A;
    int32_t cam_right = rl::KEY_D;
    int32_t cam_up = rl::KEY_W;
    int32_t cam_down = rl::KEY_S;
    
    int32_t mouse_sel = rl::MOUSE_LEFT_BUTTON;
    int32_t mouse_exp = rl::MOUSE_RIGHT_BUTTON;

public:
    // TODO: add getter setters
    Controller();
    ~Controller();
    
    void handle_input();
};

#endif // CONTROL_HPP_