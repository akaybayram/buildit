#ifndef TILE_HPP__
#define TILE_HPP__

#include "../inc/clib.hpp"

enum class TileType{
    GRASS,
    ROAD,
    TILE_TYPE_SIZE
};

class Tile{
private:
    rl::Vector2 pos;    
    uint32_t id;
    TileType type;
public:
    Tile(rl::Vector2 pos, uint32_t id, TileType type);
    ~Tile();
    void update();
    void draw();
    
    TileType get_type();
};
#endif // TILE_HPP__
