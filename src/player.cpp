#include "../inc/player.hpp"
#include "../inc/util.hpp"
#include "../inc/game.hpp"

void Player::handle_controls(){
    Game *g = g->get_instance();
    this->controller.handle_input();
    this->mouse_pos = isometric_to_cartesian(rl::GetMousePosition());
}

void Player::make_updates(){
    // call in update procedure

}

Player::Player(){}

Player::~Player(){
//    delete this->sel;
}
