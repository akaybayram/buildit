#include "../inc/entity.hpp"
#include "../inc/game.hpp"
#include "../inc/util.hpp"

Entity::Entity(rl::Vector2 pos, EntityType type){
    Game *g = g->get_instance();
    this->pos = pos;
    this->type = type;
    this->state = new FloatingState(this);
    g->entities.push_back(this);
}

Entity::~Entity(){

}

void Entity::update(float dt){
    this->state->update(dt);
}

void Entity::draw(){
    this->state->draw();
}

EntityState::EntityState(Entity *en){
    this->entity = en;
}

EntityState::~EntityState() {}

void FloatingState::update(float dt){
    (void)dt;
    Game *g = g->get_instance();
    // Update entity pos to mouse pos
    Player *self = g->players[g->get_player_id()];
    this->entity->pos = self->mouse_pos;
}

void FloatingState::draw(){
    Game *g = g->get_instance();
    rl::Vector2 calc = cartesian_to_isometric(this->entity->pos);
    rl::Color tparent = g->get_color_tint();
    tparent.a = 200.0f;
    rl::DrawTextureEx(g->entity_assets[static_cast<int>(this->entity->type)],
                      {
                          calc.x - 64, // FIXME fix magical numbers
                          calc.y - 32
                      },
                      0,
                      g->get_scale(),
                      tparent);
}
