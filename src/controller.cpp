#include "../inc/controller.hpp"
#include "../inc/game.hpp"
#include <cstdio>

Controller::Controller(){

}

Controller::~Controller(){

}

void Controller::handle_input(){
    Game *g = g->get_instance();
    
    // Camara movement
    rl::Vector2 cam_vel = {};
    cam_vel.x = float(int(rl::IsKeyDown(this->cam_right)) - int(rl::IsKeyDown(this->cam_left)));
    cam_vel.y = float(int(rl::IsKeyDown(this->cam_down)) - int(rl::IsKeyDown(this->cam_up)));
    g->cam.set_vel(cam_vel);
}