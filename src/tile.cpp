#include "../inc/game.hpp"
#include "../inc/tile.hpp"
#include "../inc/util.hpp"

Tile::Tile(rl::Vector2 pos, uint32_t id, TileType type){
    this->type = type;
    this->pos = pos;
    this->id = id;
}

Tile::~Tile(){

}

void Tile::update(){
    
}

void Tile::draw(){
    Game *g = g->get_instance();
    rl::DrawTextureEx(
        g->tile_assets[static_cast<int>(this->get_type())], 
        cartesian_to_isometric(this->pos) - g->cam.get_pos(), 0, 
        g->get_scale(), 
        g->get_color_tint());
}

TileType Tile::get_type(){
    return this->type;
}
