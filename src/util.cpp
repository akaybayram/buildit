#include "../inc/util.hpp"

rl::Vector2 cartesian_to_isometric(rl::Vector2 cart_coord){
    rl::Vector2 res;
    res.x = cart_coord.x - cart_coord.y;
    res.y = (cart_coord.x + cart_coord.y) / 2;
    
    return res;
}

rl::Vector2 isometric_to_cartesian(rl::Vector2 iso_coord){
    rl::Vector2 res;
    res.x = (2 * iso_coord.y + iso_coord.x) / 2 ;
    res.y = (2 * iso_coord.y - iso_coord.x) / 2;
    
    return res;
}

rl::Vector2 operator-(rl::Vector2 a, rl::Vector2 b){
    a.x = a.x - b.x;
    a.y = a.y - b.y;
    return a;
}

rl::Vector2 operator+(rl::Vector2 a, rl::Vector2 b){
    a.x = a.x + b.x;
    a.y = a.y + b.y;
    return a;
}

rl::Vector2 operator*(rl::Vector2 a, rl::Vector2 b){
    a.x = a.x * b.x;
    a.y = a.y * b.y;
    return a;
}

rl::Vector2 operator/(rl::Vector2 a, rl::Vector2 b){
    a.x = a.x / b.x;
    a.y = a.y / b.y;
    return a;
}