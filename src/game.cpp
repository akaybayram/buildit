#include "../inc/game.hpp"

Game *Game::instance = nullptr;

void Game::load_tile_assets(){
    this->tile_assets[static_cast<int>(TileType::GRASS)] =
        rl::LoadTexture("../assets/grass.png");

}

void Game::load_entity_assets(){
    this->entity_assets[static_cast<int>(EntityType::SELECTION)] =
        rl::LoadTexture("../assets/selected_tile.png");
    this->entity_assets[static_cast<int>(EntityType::HOUSE)] =
        rl::LoadTexture("../assets/ev.png");
}

Game::Game()
{
}

Game *Game::get_instance(){
   if(!instance){
       instance = new Game();
   } 
   return instance;
}

void Game::init(uint32_t width, uint32_t height, std::string title){
    if(this->is_running) return;
    this->width = width;
    this->height = height;
    this->title = title;
    this->is_running = true;
    this->scale = 2;
    this->tile_dim = (rl::Vector2){32.0f, 32.0f};
    this->color_tint = (rl::Color)rl::WHITE;
    
    rl::SetTargetFPS(60); // TODO: made it changable
    rl::InitWindow(width, height, title.c_str());

    // Asset loading
    this->load_tile_assets();
    this->load_entity_assets();

    uint32_t id = this->players.size();
    Player *host = new Player();
    host->id = id;
    this->self_id = id;
    host->sel = new Entity((rl::Vector2){0.0f, 0.0f}, EntityType::SELECTION);
    this->players.push_back(std::move(host));

    // Placeholder Tiles
    for(uint32_t j = 0; j < 10; ++j){
        for(uint32_t i = 0; i < 10; ++i){
            rl::Vector2 pos = {i * this->tile_dim.x * this->scale , j * this->tile_dim.y * this->scale};
            Tile t{pos, i , TileType::GRASS};
            this->tiles.push_back(t);
        }
    }
}

void Game::mainloop()
{
    float dt{0.0f};
    while (!rl::WindowShouldClose())
    {
        dt = rl::GetFrameTime();
        this->manage_players();
        this->update(dt);
        this->draw();
    }
}

void Game::manage_players(){
    // Before update handling
    for(auto *p : this->players) {
        p->handle_controls();
    }
}

void Game::update_players(){
    for(auto p: this->players){
       p->make_updates();
    }
}

void Game::update(float dt){
    this->update_players();
    for(auto e: this->entities){
        e->update(dt);
    }
    this->cam.update(dt);
}

void Game::draw()
{
    rl::BeginDrawing();
    rl::ClearBackground(rl::BLACK);
    for(auto t : this->tiles){
        t.draw();
    }
    for(auto e : this->entities){
        e->draw();
    }
    rl::EndDrawing();
}

float Game::get_scale(){
    return this->scale;
}

rl::Color Game::get_color_tint(){
    return this->color_tint;
}

uint32_t Game::get_player_id(){
    return this->self_id;
}

Game::~Game()
{
    delete this->instance;
    rl::CloseWindow();
}
