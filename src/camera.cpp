#include "../inc/camera.hpp"

Camera::Camera(){
    this->pos = {};
    this->vel = {};
    this->speed = 300.0f;
}

Camera::~Camera(){

}

void Camera::update(float dt){
    this->pos.x += this->vel.x * this->speed * dt;
    this->pos.y += this->vel.y * this->speed * dt; 
}

void Camera::set_vel(rl::Vector2 v){
    this->vel.x = v.x;
    this->vel.y = v.y;
}

rl::Vector2 Camera::get_pos(){
    return this->pos;
}